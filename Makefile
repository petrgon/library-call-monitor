SYS	= $(shell uname -s)
MAKE = make
CC = gcc
CFLAGS = -g -Wall -pedantic -I$(IDIR)
LDFLAGS	=

IDIR = include
TDIR = test
ODIR = build
LDIR = lib
SDIR = src

LIBS = -ldl

_DEPS = macros.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_SRC = _dirent.c _fcntl.c _unistd.c _stat.c _stdio.c
SRC =  $(patsubst %,$(SDIR)/%,$(_SRC))

.PHONY: all clean test

all: fsmon.so

$(ODIR):
	mkdir $@

fsmon.so: $(SRC) $(DEPS) | $(ODIR)
	$(CC) -o $@ -shared -fPIC $^ $(LDFLAGS) $(LIBS)

test: fsmon.so
	$(MAKE) -C test
	$(MAKE) -C test wget cat ls

clean:
	rm -rf $(ODIR) fsmon.so 
	$(MAKE) -C test clean
