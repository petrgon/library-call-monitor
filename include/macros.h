#define troughcall(ret_type, name, ...)                     \
    if (old_##name == NULL)                                 \
    {                                                       \
        void *handle = dlopen("libc.so.6", RTLD_LAZY);      \
        if (handle != NULL)                                 \
            *(void **)(&old_##name) = dlsym(handle, #name); \
    }                                                       \
    if (old_##name == NULL)                                 \
        exit(EXIT_FAILURE);                                 \
    ret_type ret = old_##name(__VA_ARGS__);

#define init(name)                                          \
    if (old_##name == NULL)                                 \
    {                                                       \
        void *handle = dlopen("libc.so.6", RTLD_LAZY);      \
        if (handle != NULL)                                 \
            *(void **)(&old_##name) = dlsym(handle, #name); \
    }                                                       \
    if (old_##name == NULL)                                 \
        exit(EXIT_FAILURE);

#define print(print, ...)                        \
    const char *out = getenv("MONITOR_OUTPUT");  \
    if (out == NULL || out[0] == '\0')           \
    {                                            \
        fprintf_rev(stderr, print, __VA_ARGS__); \
    }                                            \
    else                                         \
    {                                            \
        FILE *fp = fopen_rev(out, "a+");         \
        fprintf_rev(fp, print, __VA_ARGS__);     \
        fclose_rev(fp);                          \
    }                                            \
    return ret;

#define fdno(fd, id)                           \
    char path##id[1024];                       \
    char result##id[1024];                     \
    sprintf(path##id, "/dev/fd/%d", fd);       \
    memset(result##id, 0, sizeof(result##id)); \
    readlink_rev(path##id, result##id, sizeof(result##id) - 1);
