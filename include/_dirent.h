#ifndef INJECT_DIRENT_H
#define INJECT_DIRENT_H

#include "_stdio.h"
#include "_unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>

#include "macros.h"

static int (*old_closedir)(DIR *__dirp) = NULL;
static DIR *(*old_opendir)(const char *__name) = NULL;
static struct dirent *(*old_readdir)(DIR *__dirp) = NULL;

extern int closedir(DIR *__dirp) __nonnull((1));
extern DIR *opendir(const char *__name) __nonnull((1));
extern struct dirent *readdir(DIR *__dirp) __nonnull((1));

#endif //INJECT_DIRENT_H
