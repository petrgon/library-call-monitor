#ifndef INJECT_UNISTD_H
#define INJECT_UNISTD_H

#include "_stdio.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <unistd.h>
#include <sys/stat.h>

#include "macros.h"

static ssize_t (*old_read)(int __fd, void *__buf, size_t __nbytes) = NULL;
static ssize_t (*old_write)(int __fd, const void *__buf, size_t __n) = NULL;
static int (*old_dup)(int __fd) = NULL;
static int (*old_dup2)(int __fd, int __fd2) = NULL;
static int (*old_close)(int __fd) = NULL;
static ssize_t (*old_pwrite)(int __fd, const void *__buf, size_t __n, __off_t __offset) = NULL;
static int (*old_chdir)(const char *__path) = NULL;
static int (*old_chown)(const char *__file, __uid_t __owner, __gid_t __group) = NULL;
static int (*old_link)(const char *__from, const char *__to) = NULL;
static int (*old_unlink)(const char *__name) = NULL;
static ssize_t (*old_readlink)(const char *__restrict __path, char *__restrict __buf, size_t __len) = NULL;
static int (*old_symlink)(const char *__from, const char *__to) = NULL;
static int (*old_rmdir)(const char *__path) = NULL;

extern ssize_t read(int __fd, void *__buf, size_t __nbytes) __wur;
extern ssize_t write(int __fd, const void *__buf, size_t __n) __wur;
extern int dup(int __fd) __THROW __wur;
extern int dup2(int __fd, int __fd2) __THROW;
extern int close(int __fd);
extern ssize_t pwrite(int __fd, const void *__buf, size_t __n, __off_t __offset) __wur;
extern int chdir(const char *__path) __THROW __nonnull((1)) __wur;
extern int chown(const char *__file, __uid_t __owner, __gid_t __group) __THROW __nonnull((1)) __wur;
extern int link(const char *__from, const char *__to) __THROW __nonnull((1, 2)) __wur;
extern int unlink(const char *__name) __THROW __nonnull((1));
extern ssize_t readlink(const char *__restrict __path, char *__restrict __buf, size_t __len) __THROW __nonnull((1, 2)) __wur;
extern int symlink(const char *__from, const char *__to) __THROW __nonnull((1, 2)) __wur;
extern int rmdir (const char *__path) __THROW __nonnull ((1));

extern ssize_t readlink_rev(const char *__restrict __path, char *__restrict __buf, size_t __len) __THROW __nonnull((1, 2)) __wur;

#endif //INJECT_UNISTD_H
