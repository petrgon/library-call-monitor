#ifndef INJECT_STAT_H
#define INJECT_STAT_H

#include "_stdio.h"
#include "_unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <sys/stat.h>
#include "macros.h"

struct stat *p;

static int (*old_lstat)(const char *__restrict __file, struct stat *__restrict __buf) = NULL;
static int (*old_stat)(const char *__restrict __file, struct stat *__restrict __buf) = NULL;
static int (*old___xstat)(int __ver, const char *__filename, struct stat *__stat_buf) = NULL;
static int (*old___lxstat)(int __ver, const char *__filename,struct stat *__stat_buf) = NULL;
static int (*old_chmod)(const char *__file, __mode_t __mode) = NULL;
static int (*old_mkdir)(const char *__path, __mode_t __mode) = NULL;

extern int lstat(const char *__restrict __file, struct stat *__restrict __buf) __THROW __nonnull((1, 2));
extern int stat(const char *__restrict __file, struct stat *__restrict __buf) __THROW __nonnull((1, 2));
extern int __xstat (int __ver, const char *__filename, struct stat *__stat_buf) __THROW __nonnull ((2, 3));
extern int __lxstat (int __ver, const char *__filename,struct stat *__stat_buf) __THROW __nonnull ((2, 3));
extern int chmod (const char *__file, __mode_t __mode) __THROW __nonnull ((1));
extern int mkdir (const char *__path, __mode_t __mode)__THROW __nonnull ((1));

#endif //INJECT_STAT_H