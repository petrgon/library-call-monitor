#ifndef INJECT_STDIO_H
#define INJECT_STDIO_H

#include "_unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <unistd.h>
#include <stdarg.h>

#include "macros.h"

static FILE *(*old_fopen)(const char *__restrict __filename, const char *__restrict __modes) = NULL;
static int (*old_fclose)(FILE *__stream) = NULL;
static size_t (*old_fread)(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) = NULL;
static size_t (*old_fwrite)(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __s) = NULL;
static int (*old_fgetc)(FILE *__stream) = NULL;
static char *(*old_fgets)(char *__restrict __s, int __n, FILE *__restrict __stream) = NULL;
static int (*old_vfscanf)(FILE *__restrict __stream, const char *__restrict __format, ...) = NULL;
static int (*old_vfprintf)(FILE *__restrict __stream, const char *__restrict __format, ...) = NULL;
static int (*old_remove)(const char *__filename) = NULL;
static int (*old_rename)(const char *__old, const char *__new) = NULL;
static int (*old_fflush)(FILE *__stream) = NULL;
static int (*old_fflush_unlocked)(FILE *__stream) = NULL;
static size_t (*old_fread_unlocked)(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) = NULL;
static size_t (*old_fwrite_unlocked)(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) = NULL;
static char * (*old_fgets_unlocked)(char *__restrict __s, int __n, FILE *__restrict __stream) = NULL;
static int (*old_fgetc_unlocked)(FILE *__stream) = NULL;

extern FILE *fopen(const char *__restrict __filename, const char *__restrict __modes) __wur;
extern int fclose(FILE *__stream);
extern size_t fread(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) __wur;
extern size_t fwrite(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __s);
extern int fgetc(FILE *__stream);
extern char *fgets(char *__restrict __s, int __n, FILE *__restrict __stream) __wur;
extern int fscanf(FILE *__restrict __stream, const char *__restrict __format, ...) __wur;
extern int fprintf(FILE *__stream, const char *__format, ...);
extern int remove(const char *__filename) __THROW;
extern int rename(const char *__old, const char *__new) __THROW;
extern int fflush(FILE *__stream);
extern int fflush_unlocked(FILE *__stream);
extern size_t fread_unlocked(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream) __wur;
extern size_t fwrite_unlocked(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream);
extern char *fgets_unlocked(char *__restrict __s, int __n, FILE *__restrict __stream) __wur;
extern int fgetc_unlocked(FILE *__stream);

extern int fscanf_rev(FILE *__restrict __stream, const char *__restrict __format, ...);
extern int fprintf_rev(FILE *__stream, const char *__format, ...);
extern int fclose_rev(FILE *__stream);
extern FILE *fopen_rev(const char *__restrict __filename, const char *__restrict __modes);

#endif //INJECT_STDIO_H