#ifndef INJECT_FCNTL_H
#define INJECT_FCNTL_H

#include "_stdio.h"
#include "_unistd.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <stdarg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "macros.h"

static int (*old_creat)(const char *path, mode_t mode) = NULL;
static int (*old_open)(const char *pathname, int flags, ...) = NULL;

extern int creat(const char *path, mode_t mode) __nonnull((1));
extern int open(const char *pathname, int flags, ...) __nonnull((1));

#endif //INJECT_FCNTL_H
