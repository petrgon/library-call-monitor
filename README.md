**Title:** Homework #2

**Course:** Advanced Programming in the UNIX Environment

**University:** NCTU

**Author:** Bc. Petr Gondek

**Implemented Goals:**
- [x] A monitored executable can work as usual. Your program cannot break the functions of a monitored executable.
- [x] Monitor functions listed in minimum requirements.
- [x] Provide basic list for function call parameters and return values.
- [x] Provide comprehensive list for function call parameters and return values.
- [x] Output can be configured using MONITOR_OUTPUT environmental variable.
- [x] Use Makefile to manage the building process of your program. We will not grade your program if we cannot use make command to build your program.

**Monitored Functions**
closedir
opendir
readdir
creat
open
read
write
dup
dup2
close
lstat
__xlstat
stat
__xstat
pwrite
fopen
fclose
fread
fread_unlocked
fwrite
fwrite_unlocked
fgetc
fgetc_unlocked
fgets
fgets_unlocked
fscanf
fprintf
chdir
chown
chmod
remove
rename
link
unlink
readlink
symlink
mkdir
rmdir
fflush
fflush_unlocked
