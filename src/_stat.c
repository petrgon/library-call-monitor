#include "../include/_stat.h"

extern int lstat(const char *__restrict __file, struct stat *__restrict __buf)
{
    troughcall(int, lstat, __file, __buf);
    print("# lstat(\"%s\", %p {mode=%u, size=%ld}) = %d\n", __file, __buf, __buf->st_mode, __buf->st_size, ret);
}

extern int stat(const char *__restrict __file, struct stat *__restrict __buf)
{
    troughcall(int, stat, __file, __buf);
    print("# stat(\"%s\", %p {mode=%u, size=%ld}) = %d\n", __file, __buf, __buf->st_mode, __buf->st_size, ret);
}

extern int __xstat(int __ver, const char *__filename, struct stat *__stat_buf)
{
    troughcall(int, __xstat, __ver, __filename, __stat_buf);
    print("# stat(%d, \"%s\", %p {mode=%u, size=%ld}) = %d\n", __ver, __filename, __stat_buf, __stat_buf->st_mode, __stat_buf->st_size, ret);
}
extern int __lxstat(int __ver, const char *__filename, struct stat *__stat_buf)
{
    troughcall(int, __xstat, __ver, __filename, __stat_buf);
    print("# lstat(%d, \"%s\", %p {mode=%u, size=%ld}) = %d\n", __ver, __filename, __stat_buf, __stat_buf->st_mode, __stat_buf->st_size, ret);
}

extern int chmod(const char *__file, __mode_t __mode)
{
    troughcall(int, chmod, __file, __mode);
    print("# chmod(\"%s\", %u) = %d\n", __file, __mode, ret);
}

extern int mkdir(const char *__path, __mode_t __mode)
{
    troughcall(int, mkdir, __path, __mode);
    print("# mkdir(\"%s\", %u) = %d\n", __path, __mode, ret);
}