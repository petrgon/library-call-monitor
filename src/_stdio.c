#include "../include/_stdio.h"

extern FILE *fopen(const char *__restrict __filename, const char *__restrict __modes)
{
    troughcall(FILE *, fopen, __filename, __modes);
    int fd = fileno(ret);
    fdno(fd, 0);
    print("# fopen(\"%s\", \"%s\") = \"%s\"\n", __filename, __modes, result0);
}

extern int fclose(FILE *__stream)
{
    int fd = fileno(__stream);
    fdno(fd, 0);
    troughcall(int, fclose, __stream);
    print("# fclose(\"%s\") = %d\n", result0, ret);
}

extern size_t fread(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream)
{
    troughcall(size_t, fread, __ptr, __size, __n, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fread(%p, %lu, %lu, \"%s\") = %lu\n", __ptr, __size, __n, result0, ret);
}

extern size_t fwrite(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __s)
{
    troughcall(size_t, fwrite, __ptr, __size, __n, __s);
    int fd = fileno(__s);
    fdno(fd, 0);
    print("# fwrite(%p, %lu, %lu, \"%s\") = %lu\n", __ptr, __size, __n, result0, ret);
}

extern int fgetc(FILE *__stream)
{
    troughcall(int, fgetc, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fgetc(\"%s\") = %d\n", result0, ret);
}

extern char *fgets(char *__restrict __s, int __n, FILE *__restrict __stream)
{
    troughcall(char *, fgets, __s, __n, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fgets(\"%s\", %d, \"%s\") = \"%s\"\n", __s, __n, result0, ret);
}

extern int fscanf(FILE *__restrict __stream, const char *__restrict __format, ...)
{
    va_list args;
    va_start(args, __format);
    troughcall(int, vfscanf, __stream, __format, args);
    va_end(args);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fscanf(\"%s\", \"%s\", ...) = %d\n", result0, __format, ret);
}

extern int fprintf(FILE *__stream, const char *__format, ...)
{
    va_list args;
    va_start(args, __format);
    troughcall(int, vfprintf, __stream, __format, args);
    va_end(args);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fprintf(\"%s\", \"%s\", ...) = %d\n", result0, __format, ret);
}

extern int remove(const char *__filename)
{
    troughcall(int, remove, __filename);
    print("# remove(\"%s\") = %d\n", __filename, ret);
}

extern int rename(const char *__old, const char *__new)
{
    troughcall(int, rename, __old, __new);
    print("# rename(\"%s\", \"%s\") = %d\n", __old, __new, ret);
}

extern int fflush(FILE *__stream)
{
    troughcall(int, fflush, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fflush(\"%s\") = %d\n", result0, ret);
}
extern int fflush_unlocked(FILE *__stream)
{
    troughcall(int, fflush_unlocked, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fflush_unlocked(\"%s\") = %d\n", result0, ret);
}
extern size_t fread_unlocked(void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream)
{
    troughcall(size_t, fread_unlocked, __ptr, __size, __n, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fread_unlocked(%p, %lu, %lu, \"%s\") = %lu\n", __ptr, __size, __n, result0, ret);
}
extern size_t fwrite_unlocked(const void *__restrict __ptr, size_t __size, size_t __n, FILE *__restrict __stream)
{
    troughcall(size_t, fwrite_unlocked, __ptr, __size, __n, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fwrite_unlocked(%p, %lu, %lu, \"%s\") = %lu\n", __ptr, __size, __n, result0, ret);
}
extern char *fgets_unlocked(char *__restrict __s, int __n, FILE *__restrict __stream)
{
    troughcall(char *, fgets_unlocked, __s, __n, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fgets_unlocked(\"%s\", %d, \"%s\") = \"%s\"\n", __s, __n, result0, ret);
}
extern int fgetc_unlocked(FILE *__stream)
{
    troughcall(int, fgetc_unlocked, __stream);
    int fd = fileno(__stream);
    fdno(fd, 0);
    print("# fgetc_unlocked(\"%s\") = %d\n", result0, ret);
}

extern int fscanf_rev(FILE *__restrict __stream, const char *__restrict __format, ...)
{
    va_list args;
    va_start(args, __format);
    int ret = vfscanf(__stream, __format, args);
    va_end(args);
    return ret;
}

extern int fprintf_rev(FILE *__stream, const char *__format, ...)
{
    va_list args;
    va_start(args, __format);
    int ret = vfprintf(__stream, __format, args);
    va_end(args);
    return ret;
}

extern int fclose_rev(FILE *__stream)
{
    init(fclose);
    return old_fclose(__stream);
}

extern FILE *fopen_rev(const char *__restrict __filename, const char *__restrict __modes)
{
    init(fopen);
    return old_fopen(__filename, __modes);
}
