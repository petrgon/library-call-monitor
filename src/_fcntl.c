#include "../include/_fcntl.h"

extern int creat(const char *path, mode_t mode)
{
    troughcall(int, creat, path, mode);
    print("# creat(\"%s\", %d) = %d\n", path, mode, ret);
}

extern int open(const char *pathname, int flags, ...)
{
    if (flags == O_CREAT)
    {
        va_list args;
        va_start(args, flags);
        int mode = va_arg(args, int);
        va_end(args);
        troughcall(int, open, pathname, flags, mode);
        fdno(ret, 0);
        print("# open(\"%s\", %d, %d) = \"%s\"\n", pathname, flags, mode, result0);
    }
    else
    {
        troughcall(int, open, pathname, flags);
        fdno(ret, 0);
        print("# open(\"%s\", %d) = \"%s\"\n", pathname, flags, result0);
    }
}
