#include "../include/_unistd.h"

extern ssize_t read(int __fd, void *__buf, size_t __nbytes)
{
    troughcall(ssize_t, read, __fd, __buf, __nbytes);
    fdno(__fd, 0);
    print("# read(\"%s\", %p, %lu) = %ld\n", result0, __buf, __nbytes, ret);
}
extern ssize_t write(int __fd, const void *__buf, size_t __n)
{
    troughcall(ssize_t, write, __fd, __buf, __n);
    fdno(__fd, 0);
    print("# write(\"%s\", %p, %lu) = %ld\n", result0, __buf, __n, ret);
}
extern int dup(int __fd)
{
    troughcall(int, dup, __fd);
    fdno(__fd, 0);
    print("# dup(\"%s\") = %d\n", result0, ret);
}
extern int dup2(int __fd, int __fd2)
{
    fdno(__fd, 1);
    fdno(__fd2, 2);
    troughcall(int, dup2, __fd, __fd2);
    print("# dup2(\"%s\", \"%s\") = %d\n", result1, result2, ret);
}
extern int close(int __fd)
{
    fdno(__fd, 0);
    troughcall(int, close, __fd);
    print("# close(\"%s\") = %d\n", result0, ret);
}
extern ssize_t pwrite(int __fd, const void *__buf, size_t __n, __off_t __offset)
{
    troughcall(ssize_t, pwrite, __fd, __buf, __n, __offset);
    fdno(__fd, 0);
    print("# pwrite(\"%s\", %p, %lu, %ld) = %ld\n", result0, __buf, __n, __offset, ret);
}

extern int chdir(const char *__path)
{
    troughcall(int, chdir, __path);
    print("# chdir(\"%s\") = %d\n", __path, ret);
}

extern int chown(const char *__file, __uid_t __owner, __gid_t __group)
{
    troughcall(int, chown, __file, __owner, __group);
    print("# chown(\"%s\", %u, %u) = %d\n", __file, __owner, __group, ret);
}

extern int link(const char *__from, const char *__to)
{
    troughcall(int, link, __from, __to);
    print("# link(\"%s\", \"%s\") = %d\n", __from, __to, ret);
}

extern int unlink(const char *__name)
{
    troughcall(int, unlink, __name);
    print("# unlink(\"%s\") = %d\n", __name, ret);
}

extern ssize_t readlink(const char *__restrict __path, char *__restrict __buf, size_t __len)
{
    troughcall(int, readlink, __path, __buf, __len);
    print("# readlink(\"%s\", \"%s\", %lu) = %ld\n", __path, __buf, __len, ret);
}

extern int symlink(const char *__from, const char *__to)
{
    troughcall(int, symlink, __from, __to);
    print("# symlink(\"%s\", \"%s\") = %d\n", __from, __to, ret);
}

extern int rmdir (const char *__path){
    troughcall(int, rmdir, __path);
    print("# rmdir(\"%s\") = %d\n", __path, ret);
}

extern ssize_t readlink_rev(const char *__restrict __path, char *__restrict __buf, size_t __len)
{
    init(readlink);
    return old_readlink(__path, __buf, __len);
}
