#include "../include/_dirent.h"

extern int closedir(DIR *__dirp)
{
    int fd = dirfd(__dirp);
    fdno(fd, 0);
    troughcall(int, closedir, __dirp);
    print("# closedir(\"%s\") = %d\n", result0, ret);
}

extern DIR *opendir(const char *__name)
{
    troughcall(DIR *, opendir, __name);
    int fd = dirfd(ret);
    fdno(fd, 0);
    print("# opendir(\"%s\") = \"%s\"\n", __name, result0);
}

extern struct dirent *readdir(DIR *__dirp)
{
    troughcall(struct dirent *, readdir, __dirp);
    int fd = dirfd(__dirp);
    fdno(fd, 0);
    if (ret == NULL)
    {
        print("# readdir(\"%s\") = %p\n", result0, ret);
    }
    else
    {
        print("# readdir(\"%s\") = \"%s\"\n", result0, ret->d_name);
    }
}
